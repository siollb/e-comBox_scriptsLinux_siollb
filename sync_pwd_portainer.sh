#!/bin/bash

# Synchronisation du nouveau mot de passe de Portainer avec la e-comBox

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLSTOP="\033[1;31m"    # Rouge
COLINFO="\033[0;36m"    # Cyan

STOPPER()
{
        echo -e "$COLSTOP"
        echo -e "Vous avez décidé de ne pas synchroniser le mot de passe de Portainer avec la e-comBox. Si vous avez modifié le mot de passe de Portainer, la e-comBox ne fonctionnera pas tant que les mots de passes sont différents. Vous pouvez reprendre la procédure de synchronisation quand vous voulez"
        echo -e "$1"
        echo -e "$COLTXT"
        exit 1
}


POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
          echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
          read REPONSE
          if [ -z "$REPONSE" ]; then
             REPONSE="o"
          fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
           STOPPER
        fi
}

SAISIRMDP()
{
  
read -rs MDP
if [ -n "$MDP" ]; then
   echo -e "$COLSAISIE\n"
   echo -e "Saisissez de nouveau le mot de passe." 
   read -rs MDPbis
   
   while [ "$MDP" != "$MDPbis" ]
   do
     echo -e "$COLSTOP\n" 
     echo -e "Les mots de passe ne correspondent pas. Il faut recommencer."
     echo -e "$COLSAISIE\n" 
     echo -e "Saisissez le mot de passe."
     read -rs MDP
     echo ""
     echo -e "Saisissez de nouveau le mot de passe." 
     read -rs MDPbis \n 
   done
  fi
}


echo -e "$COLTITRE"
echo "********************************************************************************"
echo "*       CONFIGURATION DE LA SYNCHRONISATION DU MOT DE PASSE DE PORTAINER       *"
echo "********************************************************************************"

echo -e "$COLDEFAUT\n"

            
echo -e "Vous venez de modifier le mot de passe \"admin\" de Portainer. Ce script va le synchroniser avec la e-comBox. $COLSAISIE\n"
echo -e "Saisissez le mot de passe (le mot de passe saisi ne s'affiche pas) ou laisser vide pour ne pas synchroniser."
SAISIRMDP

echo -e "$COLCMD"

if [ ! -n "$MDP" ]; then
   STOPPER
   else
     if [ ! -e /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer ]; then
        sed -i "s/Password: 'portnairAdmin'/Password: '$MDP'/g" /var/lib/docker/volumes/ecombox_data/_data/main.js
        sed -i "s/Password: 'portnairAdmin'/Password: '$MDP'/g" /var/lib/docker/volumes/ecombox_data/_data/main.js.map
        else
           ANCIEN_MDP=`cat /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer`
           sed -i "s/Password: '$ANCIEN_MDP'/Password: '$MDP'/g" /var/lib/docker/volumes/ecombox_data/_data/main.js
           sed -i "s/Password: '$ANCIEN_MDP'/Password: '$MDP'/g" /var/lib/docker/volumes/ecombox_data/_data/main.js.map
     fi
     echo -e "$MDP" > /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer
fi

echo -e "$COLINFO\n"
echo -e "La synchronisation du mot de passe \"admin\" de Portainer a été réalisé."
echo -e ""
echo -e "Vous devez vider votre cache et relancer le navigateur"
echo -e "$COLCMD"

exit 0
