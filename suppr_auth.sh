#!/bin/bash

# Suppression de l'authentification

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLSTOP="\033[1;31m"    # Rouge
COLINFO="\033[0;36m"    # Cyan

STOPPER()
{
        echo -e "$COLSTOP"
        echo -e "Vous avez décidé de ne pas supprimer l'authentification pour accéder à e-comBox. Vous pourrez reprendre la procédure quand vous voulez."
        echo -e "$1"
        echo -e "$COLTXT"
        exit 1
}


POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
          echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
          read REPONSE
          if [ -z "$REPONSE" ]; then
             REPONSE="o"
          fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
           STOPPER
        fi
}

echo -e "$COLDEFAUT"
echo -e "Suppression de l'authentification"
echo -e ""
echo -e "$COLINFO\c"
echo -e "Vous avez décidé de supprimer l'authentification pour accéder à l'interface d'e-comBox."

POURSUIVRE

rm -f /var/lib/docker/volumes/ecombox_data/_data/.htaccess
rm -f /var/lib/docker/volumes/ecombox_config/_data/.auth

echo -e "$COLINFO\n"
echo -e "L'authentification pour accéder à l'interface d'e-comBox a été supprimée."
echo -e ""
echo -e "Vous pourrez relancez la procédure de configuration de l'authentification quand vous voulez en exécutant la commande suivante : bash /opt/e-comBox/ajout_auth.sh"
echo -e "$COLCMD"

exit 0




