#!/bin/bash

# Définition de la branche et de la version des applications
BRANCHE=master
VERSION_APPLI=siollb

# Configuration de l'environnement suite à un changement comme un nouveau proxy ou une modification de l'adresse IP

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLSTOP="\033[1;31m"  # Rouge
COLINFO="\033[0;36m"    # Cyan


STOPPER()
{
        echo -e "$COLSTOP"
        echo -e "STOP! Vous avez décidé de ne pas changer la configuration d'e-comBox. Vous pouvez reprendre la procédure quand vous voulez."
	echo -e "$1"
        echo -e "$COLTXT"
        exit 1
}



POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
	  echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
	  read REPONSE
          if [ -z "$REPONSE" ]; then
	     REPONSE="o"
	  fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
	   STOPPER
	fi
}

#clear
echo -e "$COLTITRE"
echo "***************************************************"
echo "*            CONFIGURATION DE E-COMBOX ET         *"
echo "*                DE SON ENVIRONNEMENT             *"
echo "***************************************************"

echo "" >> /var/log/ecombox.log
echo -e "$COLTITRE"
echo "Re-configuration d'e-comBox le `date`" >> /var/log/ecombox.log
echo "" >> /var/log/ecombox.log

echo "" >> /var/log/ecombox-error.log
echo -e "$COLTITRE"
echo "Re-configuration d'e-comBox le `date`" >> /var/log/ecombox-error.log
echo "" >> /var/log/ecombox-error.log

echo -e "$COLCMD"

# Appel du fichier de paramètres
source /opt/e-comBox/param.conf

#Gestion de l'adresse IP
echo -e "$COLPARTIE"
echo -e "Configuration de l'adresse IP"

echo -e "$COLTXT"
echo -e "L'adresse IP (ou le nom de domaine pleinement qualifié) qui sera utilisée pour chaque site créé est actuellement configurée à ${COLCHOIX}$ADRESSE_IP${COLTXT}. Vous pouvez valider ce choix ou bien saisir une autre valeur.${COLINFO}\n"
echo -e "ATTENTION, si le serveur doit être accessible de l'extérieur, la valeur doit correspondre à une adresse IP publique ou un nom de domaine pleinement qualifié, sinon l'adresse IP privée suffit.${COLSAISIE}\n"
echo -e "Validez l'adresse IP (ou le nom de domaine) proposée sinon saisissez une autre valeur : ${COLCHOIX}$ADRESSE_IP"
read ADRESSE_IP_SAISIE

if [ -z "$ADRESSE_IP_SAISIE" ]; then
  echo -e "$COLINFO"
  echo -e "Vous avez maintenu la valeur ${COLCHOIX}$ADRESSE_IP."
  else
     echo -e "$COLINFO"
     echo -e "La valeur saisie va être mise à jour dans le fichier de configuration /opt/e-comBox/param.conf"
     sed -e '/^#/d' -i "s/ADRESSE_IP=$ADRESSE_IP/ADRESSE_IP=$ADRESSE_IP_SAISIE/g" /opt/e-comBox/param.conf
     ADRESSE_IP=$ADRESSE_IP_SAISIE 
fi

# Gestion des ports
echo -e "$COLPARTIE"
echo -e "Configuration des ports nécessaires"

echo -e "$COLINFO"
echo -e "Les ports qui seront utilisés par défaut sont les suivants :"
echo -e ""
echo -e "Port pour l'interface de l'application : ${COLCHOIX}$PORT_ECB"
echo -e "$COLINFO"
echo -e "Port pour l'accès aux sites : ${COLCHOIX}$PORT_RP"
echo -e "$COLINFO"
echo -e "Port pour l'accès à l'interface d'administration avancée (Portainer) : ${COLCHOIX}$PORT_PORTAINER"
echo -e ""
echo -e "$COLSAISIE"
echo -e "Vous pouvez modifier ces valeurs en éditant le fichier /opt/e-comBox/param.conf puis en relançant ce script."

POURSUIVRE

#Gestion du proxy
echo -e "$COLPARTIE"
echo -e "Configuration éventuel du proxy"

if [ -z $ADRESSE_PROXY ];then
   echo -e "$COLINFO"
   echo -e "Vous n'avez pas configuré de proxy pour e-comBox.${COLTXT}"
   echo -e "Confirmez-vous ce choix (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
   read REPONSE
   if [ -z "$REPONSE" ]; then
         REPONSE="o"
   fi
   else
      echo -e "$COLINFO"
      echo -e "L'adresse de proxy actuellement configurée est ${COLCHOIX}${ADRESSE_PROXY}${COLTXT}."
      echo -e "Confirmez-vous cette adresse de proxy pour vous connecter à internet (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
      read REPONSE
      if [ -z "$REPONSE" ]; then
         REPONSE="o"
      fi          
fi
      

if [ "$REPONSE" != "o" ]; then
   echo -e "$COLTXT"
   echo -e "Saisissez l'adresse du proxy : [${COLDEFAUT}${http_proxy}${COLTXT}] ${COLSAISIE}\c"
   echo -e "(Validez l'adresse du proxy proposée sinon saisir ip-proxy:port) :"
   read ADRESSE_PROXY_SAISIE
   if [ -z "$ADRESSE_PROXY_SAISIE" ]; then
      ADRESSE_PROXY_SAISIE="$http_proxy"
      else
        export http_proxy="$ADRESSE_PROXY_SAISIE"
        export https_proxy="$ADRESSE_PROXY_SAISIE"
   fi 
   sed -e '/^#/d' -i "s/.*ADRESSE_PROXY=.*/ADRESSE_PROXY=$ADRESSE_PROXY_SAISIE/g" /opt/e-comBox/param.conf
   echo -e "$COLTXT"
   echo -e "Saisissez les hôtes à ignorer par le proxy : [${COLDEFAUT}${NO_PROXY}${COLTXT}] ${COLSAISIE}\c"
   echo -e "(Validez les hôtes à ignorer ou saisissez les hôtes séparés par une virgule (les caractères spéciaux comme \".\" ou \"*\" sont acceptés). Pour éventuellement remettre à blanc le contenu, saisissez ${COLDEFAUT}\"n\"${COLTXT} :"
   read NO_PROXY_SAISI
   if [ ! -z "$NO_PROXY_SAISI" ] && [ "$NO_PROXY_SAISI" != "n" ]; then
      sed -e '/^#/d' -i "s/.*NO_PROXY=.*/NO_PROXY=$NO_PROXY_SAISI/g" /opt/e-comBox/param.conf
      else if  [ "$NO_PROXY_SAISI" = "n" ]; then
           sed -e '/^#/d' -i "s/.*NO_PROXY=.*/NO_PROXY=/g" /opt/e-comBox/param.conf
           fi
   fi
fi


# Recharge du fichier de paramètres
source /opt/e-comBox/param.conf

# Affichage des informations
echo -e "$COLTXT"
echo -e "Vous vous apprêtez à utiliser les paramètres suivants:"
echo -e "$COLINFO"
echo -e "  - Adresse IP ou nom de domaine : $ADRESSE_IP"
echo -e "  - Port e-combox : $PORT_ECB"
echo -e "  - Port pour l'accessibilité des sites : $PORT_RP"
echo -e "  - Port Portainer : $PORT_PORTAINER"
echo -e "  - Proxy : $ADRESSE_PROXY"
echo -e "  - No Proxy : $NO_PROXY"
echo -e "$COLCMD"

echo "L'adresse IP ou le nom de domaine utilisé est $ADRESSE_IP" >> /var/log/ecombox.log
echo "" >> /var/log/ecombox.log

POURSUIVRE

# Création du fichier config.json s'il n'existe pas
if [ ! -e ~/.docker ]; then
       mkdir ~/.docker
       echo -e "$COLDEFAUT"
fi

if [ ! -e ~/.docker/config.json ]; then
       echo -e "Création d'un fichier vide config.json"
       echo -e "$COLCMD\c"
       echo "" > ~/.docker/config.json
       chown -R $USER:docker ~/.docker
       chmod g+rw ~/.docker/config.json
fi


#Configuration du proxy pour GIT et pour Docker

if [ "$ADRESSE_PROXY" != "" ]; then
   echo -e "$COLDEFAUT"
   echo -e "Congiguration de GIT pour le proxy"
   echo -e "$COLCMD\c"
   git config --global http.proxy $ADRESSE_PROXY
   if [ ! -d "/etc/systemd/system/docker.service.d" ]; then
          mkdir /etc/systemd/system/docker.service.d
   fi
   echo -e "$COLDEFAUT"
   echo "Ajout des variables d'environnement à systemd (/etc/systemd/system/docker.service.d/http-proxy.conf)"
   echo -e "$COLCMD\c"
   echo "[Service]" > /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"HTTP_PROXY=http://$ADRESSE_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"HTTPS_PROXY=http://$ADRESSE_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"NO_PROXY=$NO_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo ""
   echo -e "Redémarrage de Docker"
   systemctl daemon-reload
   systemctl restart docker
   echo -e "$COLDEFAUT"
   echo "Ajout des paramètres du Proxy dans ~/.docker/config.json après sauvegarde du fichier d'origine et du fichier actuel"
   echo -e "$COLCMD\c"
   # Sauvegarde du fichier config.json d'origine si ce n'est déjà fait
   if [ ! -e ~/.docker/config.json.ori ]; then
       cp ~/.docker/config.json ~/.docker/config.json.ori
   fi
   # Sauvegarde du fichier actuel
   cp ~/.docker/config.json ~/.docker/config.json.sauv
   # Ajout des paramètres du proxy au fichier config.json
   if [ `sed -n '$=' /root/.docker/config.json` = 1 ]; then  
   sed -i "1i \ {\n\t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
   else
    sed -i.bak "/\"proxies\": {/,10d" ~/.docker/config.json
    if [ `sed -n '$=' /root/.docker/config.json` = 2 ]; then
       sed -i "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
       else
         sed -i.bak "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n\t  }\n\t},\n" ~/.docker/config.json
       fi
    fi
     else
        echo -e "$COLINFO"
        echo -e "Aucun proxy configuré sur le système."
        echo -e "Les paramètres du proxy, s'ils existent, sont supprimés"
        echo -e "$COLCMD"
        git config --global --unset http.proxy
        if [ -f "/etc/systemd/system/docker.service.d/http-proxy.conf" ]; then
           rm -f /etc/systemd/system/docker.service.d/http-proxy.conf
           systemctl daemon-reload
           systemctl restart docker
        fi
        # Sauvegarde du fichier actuel
        cp ~/.docker/config.json ~/.docker/config.json.sauv
        # Suppression des paramètres du proxy du fichier config.json
        sed -i.bak "/\"proxies\": {/,9d" ~/.docker/config.json
	if [ `sed -n '$=' /root/.docker/config.json` = 1 ]; then
                echo "Le fichier config.json est supprimé"
		rm -rf ~/.docker/config.json
        fi
fi

# Portainer

#Configuration de l'adresse IP dans Portainer
echo -e "$COLPARTIE"
echo -e "Arrêt de Portainer"
echo -e "$COLCMD"

cd /opt/e-comBox/e-comBox_portainer
docker-compose down

# Récupération de l'ancienne IP et de l'ancien port avant changement
ANCIENNE_IP=`cat /opt/e-comBox/e-comBox_portainer/.env | grep "URL" | cut -d"=" -f2`
ANCIEN_PORT=`cat /opt/e-comBox/e-comBox_portainer/.env | grep "PORT" | cut -d"=" -f2`

echo -e "$COLDEFAUT"
echo -e "Mise à jour de /opt/e-comBox/e-comBox_portainer/.env"
echo -e "$COLCMD"

URL_UTILE=$ADRESSE_IP

echo -e "$COLCMD\c"
echo -e "URL_UTILE=$URL_UTILE" > /opt/e-comBox/e-comBox_portainer/.env
echo -e "PORT=$PORT_PORTAINER" >> /opt/e-comBox/e-comBox_portainer/.env
echo ""

# Lancement de Portainer
echo -e "$COLPARTIE"
echo -e "Lancement de portainer"
echo -e "$COLCMD\c"
cd /opt/e-comBox/e-comBox_portainer/
docker-compose up -d

if [ $? <> 0 ]; then
  echo -e "Portainer a été lancé." >> /var/log/ecombox.log
  echo "" >> /var/log/ecombox.log
  else
    echo -e "Portainer n'a pas pu être lancé." >> /var/log/ecombox-error.log
    echo "" >> /var/log/ecombox.log
fi

echo -e "$COLINFO"
echo "Portainer est accessible à l'URL suivante :"
echo -e "${COLCHOIX}http://$URL_UTILE:$PORT_PORTAINER/portainer/"
echo -e "$COLCMD\n"


# Reverse Proxy Nginx

#Configuration de l'adresse IP

echo -e "$COLPARTIE"
echo -e "Arrêt du reverse proxy"
echo -e "$COLCMD"

cd /opt/e-comBox/e-comBox_reverseproxy
docker-compose down

echo -e "$COLDEFAUT"
echo "Mise à jour de /opt/e-comBox/e-comBox_reverseproxy/.env."
echo -e "$COLCMD"
echo -e "URL_UTILE=$URL_UTILE" > /opt/e-comBox/e-comBox_reverseproxy/.env
echo -e "NGINX_PORT=$PORT_RP" >> /opt/e-comBox/e-comBox_reverseproxy/.env
echo ""


# Lancement du reverse proxy
echo -e "$COLPARTIE"
echo "Lancement du reverse proxy"
echo -e "$COLCMD\c"
cd /opt/e-comBox/e-comBox_reverseproxy/
docker-compose up -d 2>> /var/log/ecombox-error.log

if [ $? <> 0 ]; then
  echo -e "Le reverse proxy a été lancé." >> /var/log/ecombox.log
  echo "" >> /var/log/ecombox.log
  else
    echo -e "Le reverse proxy n'a pas pu être lancé." >> /var/log/ecombox-error.log
    echo "" >> /var/log/ecombox.log
fi

echo -e "$COLINFO"
echo "Les sites seront accessibles via des URL formées de la manière suivante :"
echo -e "http://$URL_UTILE:$PORT_RP/nom_du_site/"
echo -e "$COLCMD\n"


# Redémarrage de l'application

echo -e "$COLPARTIE"
echo -e "Redémarrage de l'application"
echo -e "$COLCMD"
echo -e ""

# Arrêt
if docker ps -a | grep e-combox; then
	docker rm -f e-combox
fi

# Lancement de e-comBox
echo -e "$COLPARTIE"
echo "Lancement et configuration de l'environnement de l'application e-comBox"
echo -e "$COLCMD\c"
echo -e ""
docker run -dit --name e-combox -v ecombox_data:/usr/local/apache2/htdocs/ -v ecombox_config:/etc/ecombox-conf --restart always -p $PORT_ECB:80 --network bridge_e-combox reseaucerta/e-combox:$VERSION_APPLI 2>> /var/log/ecombox-error.log

if [ $? <> 0 ]; then
  echo -e "e-comBox a été lancée." >> /var/log/ecombox.log
  echo "" >> /var/log/ecombox.log
  else  
    echo -e "e-comBox n'a pas pu être lancée." >> /var/log/ecombox-error.log
    echo "" >> /var/log/ecombox.log
fi

# Configuration de l'API 
if [ "$ANCIENNE_IP:$ANCIEN_PORT" != "$URL_UTILE:$PORT_PORTAINER" ]; then
   echo -e ""
   echo -e "$COLINFO"
   echo -e "L'URL $ANCIENNE_IP:$ANCIEN_PORT est remplacée par $URL_UTILE:$PORT_PORTAINER."
   echo -e "$COLCMD\n"
   echo -e "L'URL $ANCIENNE_URL est remplacée par $URL_UTILE:$PORT_PORTAINER." >> /var/log/ecombox.log
 
   for fichier in /var/lib/docker/volumes/ecombox_data/_data/*.js /var/lib/docker/volumes/ecombox_data/_data/*.js.map
   do
     sed -i -e "s/$ANCIENNE_IP:$ANCIEN_PORT/$URL_UTILE:$PORT_PORTAINER/g" $fichier
   done
   else
     echo -e ""
     echo -e "$COLINFO"
     echo -e "Aucun changement à opérer au niveau de $URL_UTILE:$PORT_PORTAINER."
     echo -e "$COLCMD\n"
     echo -e "Aucun changement à opérer au niveau de $URL_UTILE:$PORT_PORTAINER." >> /var/log/ecombox.log
fi


# Arrêt des containers qui doivent être redémarrés après reconfiguration de l'environnement
echo -e "$COLINFO"
echo "Les conteneurs qui doivent être obligatoirement redémarrés après reconfiguration de l'environnement sont arrêtés."

LIST_CONTAINERS=`docker ps -q -f name=prestashop && docker ps -q -f name=woocommerce && docker ps -q -f name=blog & docker ps -q -f name=kanboard`

if [ -n "$LIST_CONTAINERS" ]; then
        docker stop $LIST_CONTAINERS
fi


echo -e "$COLINFO"
echo -e "L'application e-comBox est maintenant accessible à l'URL suivante :"
echo -e "${COLCHOIX}http://$URL_UTILE:$PORT_ECB"
echo -e "$COLCMD"

exit 0



