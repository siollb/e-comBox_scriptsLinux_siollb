#!/bin/bash

# Définition de la branche et de la version des applications
BRANCHE=master
VERSION_APPLI=siollbv2

# Installation et configuration d'e-combox sous réserve d'acceptation de la licence
# Les fichiers incluant le docker-compose seront téléchargés dans /opt/e-comBox

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLSTOP="\033[1;31m"    # Rouge
COLINFO="\033[0;36m"    # Cyan


STOPPER()
{
        echo -e "$COLSTOP"
        echo -e "Vous avez décidé de ne pas continuer à installer et configurer e-comBox. Vous pouvez reprendre la procédure quand vous voulez."
	echo -e "$1"
        echo -e "$COLTXT"
        exit 1
}



POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
	  echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
	  read REPONSE
          if [ -z "$REPONSE" ]; then
	     REPONSE="o"
	  fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
	   STOPPER
	fi
}

initInstall ()
{
# Téléchargement de la licence CeCILL
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/licenceCeCILL.txt -o licenceCeCILL.txt
echo -e ""
echo -e "$COLINFO"
echo -e "Vous devez lire et accepter les termes de la licence CeCILL avant d'installer (ou de mettre à jour) et de pouvoir utiliser e-comBox $VERSION_APPLI."
echo -e "$COLCMD"
echo -e "Appuyer sur n'importe quelle touche pour lire la licence CeCILL."
echo -e ""
if [ ! -e licenceCeCILL.txt ]; then
   echo -e "$COLINFO"
   echo -e "Le fichier contenant les termes de la licence n'a pas pu être téléchargé et rangé dans le répertoire /opt/e-comBox."
   echo -e "Vérifiez la configuration de votre proxy pour l'outil curl."
   echo -e "Éventuellement, téléchargez le fichier par vos propres moyens via l'URL suivante https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/licenceCeCILL.txt et rangez-le dans le dossier /opt/e-comBox."
   echo -e ""
   echo -e "Relancez-ensuite le script : bash /opt/e-comBox/configure_application.sh."
   echo -e ""
   echo -e "Appuyer sur n'importe quelle touche pour arrêter le script."
   read entree
   else
     read contenu
     more licenceCeCILL.txt
     echo -e "Acceptez-vous les termes de la licence CeCCILL (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
     echo -e ""
     read saisie
     if [ "$saisie" = n ]; then
        STOPPER
        else installer
     fi
fi
}

installer ()
{
clear
echo -e "$COLTITRE"
echo "***************************************************"
echo "*  INSTALLATION DE E-COMBOX V2 ET CONFIGURATION   *"
echo "*                DE SON ENVIRONNEMENT             *"
echo "***************************************************"

# Création d'un fichier de log
echo -e "$COLINFO"
echo -e "Création d'un fichier de log : /var/log/ecombox.log"
echo -e "$COLTITRE"
echo "Configuration d'e-comBox le `date`" > /var/log/ecombox.log
echo "" >> /var/log/ecombox.log
echo -e "$COLCMD"

#Création du dossier d'installation de l'application s'il n'existe pas déjà
if [ ! -d "/opt/e-comBox" ]; then
   mkdir /opt/e-comBox
fi

# Bascule du fichier licence vers /opt/e-comBox
mv licenceCeCILL.txt /opt/e-comBox/ 2>> /var/log/ecombox.log

# Téléchargement du fichier contenant les paramètres s'il n'existe pas déjà
if [ ! -e "/opt/e-comBox/param.conf" ]; then
   curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/param.conf -o /opt/e-comBox/param.conf
fi

# Appel du fichier de paramètres
source /opt/e-comBox/param.conf


test_adresse_ip=`cat /opt/e-comBox/param.conf | grep -v '#' ADRESSE_IP`

if [ -z "$test_adresse_ip" ]; then
   echo "" >> /opt/e-comBox/param.conf
   echo "# Adresse IP ou nom du domaine pleinement qualifié" >> /opt/e-comBox/param.conf
   echo "ADRESSE_IP=127.0.0.1" >> /opt/e-comBox/param.conf
   ADRESSE_IP=127.0.0.1
fi

#Gestion de l'adresse IP
echo -e "$COLPARTIE"
echo -e "Configuration de l'adresse IP"

echo -e "$COLTXT"
echo -e "L'adresse IP (ou le nom de domaine pleinement qualifié) qui sera utilisée pour chaque site créé est actuellement configurée à ${COLCHOIX}$ADRESSE_IP${COLTXT}. Vous pouvez valider ce choix ou bien saisir une autre valeur.${COLINFO}\n"
echo -e "ATTENTION, si le serveur doit être accessible de l'extérieur, la valeur doit correspondre à une adresse IP publique ou un nom de domaine pleinement qualifié, sinon l'adresse IP privée suffit.${COLSAISIE}\n"
echo -e "Validez l'adresse IP (ou le nom de domaine) proposée sinon saisissez une autre valeur : ${COLCHOIX}$ADRESSE_IP"
read ADRESSE_IP_SAISIE

if [ -z "$ADRESSE_IP_SAISIE" ]; then
  echo -e "$COLINFO"
  echo -e "Vous avez maintenu la valeur ${COLCHOIX}$ADRESSE_IP."
  else
     echo -e "$COLINFO"
     echo -e "La valeur saisie va être mise à jour dans le fichier de configuration /opt/e-comBox/param.conf"
     sed -i "s/ADRESSE_IP=$ADRESSE_IP/ADRESSE_IP=$ADRESSE_IP_SAISIE/g" /opt/e-comBox/param.conf
     ADRESSE_IP=$ADRESSE_IP_SAISIE 
fi


# Gestion des ports
echo -e "$COLPARTIE"
echo -e "Configuration des ports nécessaires"

test_port_rp=`cat /opt/e-comBox/param.conf | grep -v '#' PORT_RP`

if [ -z "$test_port_rp" ]; then
   echo "" >> /opt/e-comBox/param.conf
   echo "# Port utilisé pour le reverse proxy nginx" >> /opt/e-comBox/param.conf
   echo "PORT_RP=8800" >> /opt/e-comBox/param.conf
   PORT_RP=8800
fi

echo -e "$COLINFO"
echo -e "Les ports qui seront utilisés par défaut sont les suivants :"
echo -e ""
echo -e "Port pour l'interface de l'application : ${COLCHOIX}$PORT_ECB"
echo -e "$COLINFO"
echo -e "Port pour l'accès aux sites : ${COLCHOIX}$PORT_RP"
echo -e "$COLINFO"
echo -e "Port pour l'accès à l'interface d'administration avancée (Portainer) : ${COLCHOIX}$PORT_PORTAINER"
echo -e ""
echo -e "$COLSAISIE"
echo -e "Vous pouvez modifier ces valeurs en éditant le fichier /opt/e-comBox/param.conf puis en relançant ce script."

POURSUIVRE



#Gestion du proxy
echo -e "$COLPARTIE"
echo -e "Configuration éventuel du proxy"

test_proxy=`cat /opt/e-comBox/param.conf | grep -v '#' ADRESSE_PROXY`

if [ -z "$test_proxy" ]; then
   echo "" >> /opt/e-comBox/param.conf
   echo "# Adresse du Proxy" >> /opt/e-comBox/param.conf
   echo "ADRESSE_PROXY=$http_proxy" >> /opt/e-comBox/param.conf
   ADRESSE_PROXY=$http_proxy
fi

test_no_proxy=`cat /opt/e-comBox/param.conf | grep -v '#' NO_PROXY`

if [ -z "$test_no_proxy" ]; then
   echo "" >> /opt/e-comBox/param.conf
   echo "# No proxy" >> /opt/e-comBox/param.conf
   echo "NO_PROXY=" >> /opt/e-comBox/param.conf
   NO_PROXY=""
fi


if [ -z $ADRESSE_PROXY ];then
   echo -e "$COLINFO"
   echo -e "Vous n'avez pas configuré de proxy pour e-comBox.${COLTXT}"
   echo -e "Confirmez-vous ce choix (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
   read REPONSE
   if [ -z "$REPONSE" ]; then
         REPONSE="o"
   fi
   else
      echo -e "$COLINFO"
      echo -e "L'adresse de proxy actuellement configurée est ${COLCHOIX}${ADRESSE_PROXY}${COLTXT}."
      echo -e "Confirmez-vous cette adresse de proxy pour vous connecter à internet (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
      read REPONSE
      if [ -z "$REPONSE" ]; then
         REPONSE="o"
      fi          
fi
      

if [ "$REPONSE" != "o" ]; then
   echo -e "$COLTXT"
   echo -e "Saisissez l'adresse du proxy : [${COLDEFAUT}${http_proxy}${COLTXT}] ${COLSAISIE}\c"
   echo -e "(Validez l'adresse du proxy proposée sinon saisir ip-proxy:port) :"
   read ADRESSE_PROXY_SAISIE
   if [ -z "$ADRESSE_PROXY_SAISIE" ]; then
      ADRESSE_PROXY_SAISIE="$http_proxy"
      else
        export http_proxy="$ADRESSE_PROXY_SAISIE"
        export https_proxy="$ADRESSE_PROXY_SAISIE"
   fi 
   sed -i "s/.*ADRESSE_PROXY=.*/ADRESSE_PROXY=$ADRESSE_PROXY_SAISIE/g" /opt/e-comBox/param.conf
   echo -e "$COLTXT"
   echo -e "Saisissez les hôtes à ignorer par le proxy : [${COLDEFAUT}${NO_PROXY}${COLTXT}] ${COLSAISIE}\c"
   echo -e "(Validez les hôtes à ignorer ou saisissez les hôtes séparés par une virgule (les caractères spéciaux comme \".\" ou \"*\" sont acceptés). Pour éventuellement remettre à blanc le contenu, saisissez ${COLDEFAUT}\"n\"${COLTXT} :"
   read NO_PROXY_SAISI
   if [ ! -z "$NO_PROXY_SAISI" ] && [ "$NO_PROXY_SAISI" != "n" ]; then
      sed -i "s/.*NO_PROXY=.*/NO_PROXY=$NO_PROXY_SAISI/g" /opt/e-comBox/param.conf
      else if  [ "$NO_PROXY_SAISI" = "n" ]; then
           sed -i "s/.*NO_PROXY=.*/NO_PROXY=/g" /opt/e-comBox/param.conf
           fi
   fi
fi

# Recharge du fichier de paramètres
source /opt/e-comBox/param.conf

# Affichage des informations
echo -e "$COLTXT"
echo -e "Vous vous apprêtez à utiliser les paramètres suivants:"
echo -e "$COLINFO"
echo -e "  - Adresse IP ou nom de domaine : $ADRESSE_IP"
echo -e "  - Port e-combox : $PORT_ECB"
echo -e "  - Port pour l'accessibilité des sites : $PORT_RP"
echo -e "  - Port Portainer : $PORT_PORTAINER"
echo -e "  - Proxy : $ADRESSE_PROXY"
echo -e "  - No Proxy : $NO_PROXY"
echo -e "$COLCMD"

echo "L'adresse IP ou le nom de domaine utilisé est $ADRESSE_IP" >> /var/log/ecombox.log
echo "" >> /var/log/ecombox.log

POURSUIVRE

# Création du fichier config.json s'il n'existe pas
if [ ! -e ~/.docker ]; then
       mkdir ~/.docker
       echo -e "$COLDEFAUT"
fi

if [ ! -e ~/.docker/config.json ]; then
       echo -e "Création d'un fichier vide config.json"
       echo -e "$COLCMD\c"
       echo "" > ~/.docker/config.json
       chown -R $USER:docker ~/.docker
       chmod g+rw ~/.docker/config.json
fi

#Configuration du proxy pour GIT et pour Docker

if [ "$ADRESSE_PROXY" != "" ]; then
   echo -e "$COLDEFAUT"
   echo -e "Congiguration de GIT pour le proxy"
   echo -e "$COLCMD\c"
   git config --global http.proxy $ADRESSE_PROXY
   if [ ! -d "/etc/systemd/system/docker.service.d" ]; then
          mkdir /etc/systemd/system/docker.service.d
   fi
   echo -e "$COLDEFAUT"
   echo "Ajout des variables d'environnement à systemd (/etc/systemd/system/docker.service.d/http-proxy.conf)"
   echo -e "$COLCMD\c"
   echo "[Service]" > /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"HTTP_PROXY=http://$ADRESSE_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"HTTPS_PROXY=http://$ADRESSE_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"NO_PROXY=$NO_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo ""
   echo -e "Redémarrage de Docker"
   systemctl daemon-reload
   systemctl restart docker
   echo -e "$COLDEFAUT"
   echo -e "Ajout des paramètres du Proxy dans ~/.docker/config.json après sauvegarde du fichier d'origine et du fichier actuel."
   echo -e "$COLCMD\c"
   echo "Ajout des paramètres du proxy $ADRESSE_PROXY et $NO_PROXY" >> /var/log/ecombox.log
   echo "" >> /var/log/ecombox.log
   # Sauvegarde du fichier config.json d'origine si ce n'est déjà fait
   if [ ! -e ~/.docker/config.json.ori ]; then
       cp ~/.docker/config.json ~/.docker/config.json.ori
   fi
   # Sauvegarde du fichier actuel
   cp ~/.docker/config.json ~/.docker/config.json.sauv
   # Ajout des paramètres du proxy au fichier config.json
   if [ `sed -n '$=' /root/.docker/config.json` = 1 ]; then
   sed -i "1i \ {\n\t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
   else
    sed -i.bak "/\"proxies\": {/,10d" ~/.docker/config.json
    if [ `sed -n '$=' /root/.docker/config.json` = 2 ]; then
       sed -i "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
       else
         sed -i.bak "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n\t  }\n\t},\n" ~/.docker/config.json
       fi
    fi
     else
        echo -e "$COLINFO"
        echo -e "Aucun proxy configuré sur le système."
        echo -e "Les paramètres du proxy, s'ils existent, sont supprimés."
        echo -e "$COLCMD"
	echo "Aucun proxy configuré sur le système" >> /var/log/ecombox.log
        echo "" >> /var/log/ecombox.log
        git config --global --unset http.proxy
        if [ -f "/etc/systemd/system/docker.service.d/http-proxy.conf" ]; then
           rm -f /etc/systemd/system/docker.service.d/http-proxy.conf
           systemctl daemon-reload
           systemctl restart docker
        fi
        # Sauvegarde du fichier actuel
        cp ~/.docker/config.json ~/.docker/config.json.sauv
        # Suppression des paramètres du proxy du fichier config.json
        sed -i.bak "/\"proxies\": {/,9d" ~/.docker/config.json
        if [ `sed -n '$=' /root/.docker/config.json` = 1 ]; then
                echo "Le fichier config.json est supprimé"
                rm -rf ~/.docker/config.json
        fi
fi


# Création du réseau pour l'application

echo -e "$COLPARTIE"
echo -e "Création ou modification du réseau pour e-comBox"
echo -e "$COLCMD"

if ( docker network ls | grep bridge_e-combox ); then
   NET_ECB=`docker network inspect --format='{{range .IPAM.Config}}{{.Subnet}}{{end}}' bridge_e-combox`
   echo -e "$COLINFO"
   echo -e "Le système constate que le réseau ${COLCHOIX}${NET_ECB}${COLINFO} est déjà créé."
   echo -e "Si vous désirez modifier les paramètres de ce réseau, les sites existants seront supprimés."
   echo -e ""
   echo -e "${COLTXT}Voulez-vous modifier le réseau ? $COLSAISIE\c"
   echo -e "(tapez oui pour modifier le réseau et SUPPRIMER les sites ou sur n'importe quel autre touche pour continuer)."
   read CONFIRM_RESEAU
   if [ "$CONFIRM_RESEAU" = "oui" ]; then
      docker rm -f $(docker ps -aq)
      docker volume rm $(docker volume ls -qf dangling=true)
      docker network rm bridge_e-combox
      echo -e "$COLTXT\n"
      echo "Saisissez le nouveau réseau sous la forme ${COLCHOIX}adresseIP/CIDR${COLSAISIE}."
      read NET_ECB
      echo ""
      echo "$COLCMD"
      docker network create --subnet $NET_ECB bridge_e-combox 2>> /var/log/ecombox-error.log
      echo -e "$COLINFO"
      echo -e "Le nouveau réseau ${COLCHOIX}$NET_ECB${COLINFO} a été créé."
      else
        echo -e "$COLTXT" 
        echo -e "Vous avez décidé de ne pas modifier le réseau."
   fi
   else
	echo -e "$COLINFO"
        echo -e ""
        echo -e "Le réseau d'e-comBox sera défini par défaut à ${COLCHOIX}192.168.97.0/24${COLTXT}."
        echo -e "Voulez-vous changer ce paramétrage ? $COLSAISIE\c"
        echo -e "(tapez oui pour changer l'adresse IP du réseau créé par défaut ou sur n'importe quelle touche pour continuer sans changement)."
        read CONFIRM_RESEAU
        if [ "$CONFIRM_RESEAU" = "oui" ]; then
           echo -e "$COLSAISIE\n"
           echo "Saisissez l'adresse du réseau sous la forme \"adresseIP/CIDR\"."
           read NET_ECB
           echo -e ""
           echo -e "$COLCMD"
           docker network create --subnet $NET_ECB bridge_e-combox 2>> /var/log/ecombox-error.log
           echo -e "$COLINFO"
           echo -e "Le réseau $NET_ECB a été créé."
           else
               NET_ECB=192.168.97.0/24
               echo -e ""
               echo -e "$COLCMD"
               docker network create --subnet $NET_ECB bridge_e-combox 2>> /var/log/ecombox-error.log
               echo -e "$COLINFO"
               echo -e "Le réseau ${COLCHOIX}$NET_ECB${COLINFO} a été créé."
       fi
 fi

echo -e "Le réseau ${COLCHOIX}$NET_ECB${COLINFO} est utilisé." >> /var/log/ecombox.log
echo "" >> /var/log/ecombox.log

# Portainer

#Récupération de portainer
echo -e "$COLPARTIE"
echo -e "Récupération et configuration de Portainer"
echo -e "$COLCMD"

if [ -d "/opt/e-comBox/e-comBox_portainer_siollb" ]; then
	echo -e "$COLINFO"
	echo "Portainer existe et va être remplacé."
	echo -e "$COLCMD\c"
	echo -e "Portainer existe et va être remplacé." >> /var/log/ecombox.log
        echo "" >> /var/log/ecombox.log

        cd /opt/e-comBox/e-comBox_portainer_siollb
	docker-compose down
        cd ..
	rm -rf /opt/e-comBox/e-comBox_portainer_siollb
        #docker volume rm e-combox_portainer_portainer-data 2>> /var/log/ecombox-error.log
        # Récupération éventuelle du mot de passe pour synchronisation avec e-comBox
        if [ -e /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer ]; then
           MDP_PORTAINER=`cat /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer`
        fi 
fi

cd /opt/e-comBox
git clone -b $BRANCHE https://gitlab.com/siollb/e-comBox_portainer_siollb.git 2>> /var/log/ecombox-error.log

#Configuration de l'adresse IP
echo -e "$COLINFO"
echo "Mise à jour de /opt/e-comBox/e-comBox_portainer_siollb/.env."
echo -e "$COLCMD"

URL_UTILE=$ADRESSE_IP

echo -e "$COLCMD\c"
echo -e "URL_UTILE=$URL_UTILE" > /opt/e-comBox/e-comBox_portainer_siollb/.env
echo -e "PORT=$PORT_PORTAINER" >> /opt/e-comBox/e-comBox_portainer_siollb/.env
echo ""


# Lancement de Portainer
echo -e "$COLPARTIE"
echo "Lancement de portainer"
echo -e "$COLCMD\c"
cd /opt/e-comBox/e-comBox_portainer_siollb/
docker-compose up -d 2>> /var/log/ecombox-error.log

if [ $? <> 0 ]; then
  echo -e "Portainer a été lancé." >> /var/log/ecombox.log
  echo "" >> /var/log/ecombox.log
  else
    echo -e "Portainer n'a pas pu être lancé." >> /var/log/ecombox-error.log
    echo "" >> /var/log/ecombox.log
fi

echo -e "$COLINFO"
echo "Portainer est accessible à l'URL suivante :"
echo -e "${COLCHOIX}http://$URL_UTILE:$PORT_PORTAINER/portainer/"
echo -e "$COLCMD\n"

# Reverse Proxy Nginx

#Récupération du reverse proxy
echo -e "$COLPARTIE"
echo -e "Récupération et configuration du reverse proxy"
echo -e "$COLCMD"

if [ -d "/opt/e-comBox/e-comBox_reverseproxy_siollb" ]; then
	echo -e "$COLTXT"
	echo "Le reverse proxy existe et va être remplacé."
	echo -e "$COLCMD\c"
	echo -e "Le reverse proxy existe et va être remplacé." >> /var/log/ecombox.log
        echo "" >> /var/log/ecombox.log

        cd /opt/e-comBox/e-comBox_reverseproxy_siollb
	docker-compose down
	docker image rm -f reseaucerta/docker-gen:2.1
	docker volume rm e-combox_reverseproxy_nginx-html >> /var/log/ecombox-error.log
	docker volume rm e-combox_reverseproxy_nginx-docker-gen-templates >> /var/log/ecombox-error.log
	
        cd ..
	rm -rf /opt/e-comBox/e-comBox_reverseproxy_siollb        
fi

cd /opt/e-comBox
git clone -b $BRANCHE https://gitlab.com/siollb/e-comBox_reverseproxy_siollb.git 2>> /var/log/ecombox-error.log

#Configuration de l'adresse IP
echo -e "$COLINFO"
echo "Mise à jour de /opt/e-comBox/e-comBox_reverseproxy_siollb/.env."
echo -e "$COLCMD"
echo -e "URL_UTILE=$URL_UTILE" > /opt/e-comBox/e-comBox_reverseproxy_siollb/.env
echo -e "NGINX_PORT=$PORT_RP" >> /opt/e-comBox/e-comBox_reverseproxy_siollb/.env
echo ""


# Lancement du reverse proxy
echo -e "$COLPARTIE"
echo "Lancement du reverse proxy"
echo -e "$COLCMD\c"
cd /opt/e-comBox/e-comBox_reverseproxy_siollb/
docker-compose up -d 2>> /var/log/ecombox-error.log

if [ $? <> 0 ]; then
  echo -e "Le reverse proxy a été lancé." >> /var/log/ecombox.log
  echo "" >> /var/log/ecombox.log
  else
    echo -e "Le reverse proxy n'a pas pu être lancé." >> /var/log/ecombox-error.log
    echo "" >> /var/log/ecombox.log
fi

echo -e "$COLINFO"
echo "Les sites seront accessibles via des URL formées de la manière suivante :"
echo -e "${COLCHOIX}http://$URL_UTILE:$PORT_RP/nom_du_site/"
echo -e "$COLCMD\n"


# Configuration de l'application

echo -e "$COLPARTIE"
echo -e "Suppression d'e-comBox si une version existe"
echo -e "$COLCMD"

if docker ps -a | grep e-combox; then
	docker rm -f e-combox 2>> /var/log/ecombox-error.log
	docker volume rm $(docker volume ls -qf dangling=true) 2>> /var/log/ecombox-error.log
	docker image rm -f reseaucerta/e-combox:$VERSION_APPLI
	echo -e "e-comBox existe et va être remplacée." >> /var/log/ecombox.log
        echo "" >> /var/log/ecombox.log
fi


# Récupération d'une éventuelle nouvelle version d'e-comBox
echo -e "$COLPARTIE"
echo "Récupération d'e-combox"
echo -e "$COLCMD\c"
echo -e ""

docker pull reseaucerta/e-combox:$VERSION_APPLI 2>> /var/log/ecombox-error.log

# Lancement de e-comBox
echo -e "$COLPARTIE"
echo "Lancement et configuration de l'environnement de l'application e-comBox"
echo -e "$COLCMD\c"
echo -e ""
docker run -dit --name e-combox -v ecombox_data:/usr/local/apache2/htdocs/ -v ecombox_config:/etc/ecombox-conf --restart always -p $PORT_ECB:80 --network bridge_e-combox reseaucerta/e-combox:$VERSION_APPLI 2>> /var/log/ecombox-error.log

if [ $? <> 0 ]; then
  echo -e "e-comBox a été lancée." >> /var/log/ecombox.log
  echo "" >> /var/log/ecombox.log
  else  
    echo -e "e-comBox n'a pas pu être lancée." >> /var/log/ecombox-error.log
    echo "" >> /var/log/ecombox.log
fi

# Nettoyage des anciennes images si elles existent
echo -e "$COLPARTIE"
echo "Suppression éventuelle des images si elles ne sont associées à aucun site"
echo -e "$COLCMD\c"
echo -e ""
docker image rm $(docker images -q) 2>> /var/log/ecombox-error.log

if [ `docker images -qf dangling=true` ]; then
 docker rmi $(docker images -qf dangling=true) 2>> /var/log/ecombox-error.log
fi

# Arrêt des conteneurs qui doivent être redémarrés après reconfiguration de l'environnement
echo -e "$COLINFO"
echo "Les conteneurs qui doivent être obligatoirement redémarrés après reconfiguration de l'environnement sont arrêtés."

LISTE_CONTENEURS=`docker ps -q -f name=prestashop && docker ps -q -f name=woocommerce && docker ps -q -f name=blog && docker ps -q -f name=kanboard`

if [ -n "$LISTE_CONTENEURS" ]; then
	docker stop $LISTE_CONTENEURS
fi


# Configuration de l'API
echo -e "$COLPARTIE"
echo "Configuration d'e-comBox"
echo -e "$COLCMD\c"
echo -e ""

ANCIENNE_URL=`grep -ni "var endpoint" /var/lib/docker/volumes/ecombox_data/_data/main.js | cut -d"/" -f3`

if [ "$ANCIENNE_URL" != "$URL_UTILE:$PORT_PORTAINER" ]; then
   echo -e ""
   echo -e "L'URL $ANCIENNE_URL est remplacée par $URL_UTILE:$PORT_PORTAINER." >> /var/log/ecombox.log
 
   for fichier in /var/lib/docker/volumes/ecombox_data/_data/*.js /var/lib/docker/volumes/ecombox_data/_data/*.js.map
   do
     sed -i -e "s/$ANCIENNE_URL/$URL_UTILE:$PORT_PORTAINER/g" $fichier
   done
   else
     echo -e ""
     echo -e "Aucun changement à opérer au niveau de $URL_UTILE:$PORT_PORTAINER." >> /var/log/ecombox.log
fi

if [ "$MDP_PORTAINER" != "" ]; then
   echo -e "${COLINFO}Vous aviez modifié le mot de passe \"admin\" de Portainer. Ce dernier va être synchronisé avec la e-comBox."
   echo -e "\n Vous aviez modifié le mot de passe \"admin\" de Portainer. Ce dernier va être synchronisé avec la e-comBox." >> /var/log/ecombox.log
   sed -i "s/Password: 'portnairAdmin'/Password: '$MDP_PORTAINER'/g" /var/lib/docker/volumes/ecombox_data/_data/main.js
   sed -i "s/Password: 'portnairAdmin'/Password: '$MDP_PORTAINER'/g" /var/lib/docker/volumes/ecombox_data/_data/main.js.map
   echo -e $MDP_PORTAINER > /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer
fi

echo -e "$COLTITRE"
echo "***************************************************"
echo "*        FIN DE L'INSTALLATION DE E-COMBOX        *"
echo "***************************************************"

echo -e "$COLDEFAUT"
echo "Téléchargement du fichier contenant les identifiants d'accès et des scripts permettant de reconfigurer l'application si nécessaire"
echo -e "$COLCMD\c"

# Téléchargement du fichier contenant les identifiants d'accès et les scripts utiles à la configuration
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/e-comBox_identifiants_acces_applications.pdf -o /opt/e-comBox/e-comBox_identifiants_acces_applications.pdf
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/change_config_ip.sh -o /opt/e-comBox/change_config_ip.sh
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/configure_application.sh -o /opt/e-comBox/configure_application.sh
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/sync_pwd_portainer.sh -o /opt/e-comBox/sync_pwd_portainer.sh

echo -e "$COLINFO"
echo -e "L'application e-comBox est maintenant accessible à l'URL suivante :"
echo -e "${COLCHOIX}http://$URL_UTILE:$PORT_ECB${COLINFO}"
echo -e ""
echo -e "Les identifiants d'accès figurent dans le fichier /opt/e-comBox/e-comBox_identifiants_acces_applications.pdf."
echo -e ""
echo -e "La dernière étape vous donne la possibilité de configurer une authentification pour accéder à l'interface d'e-comBox."
echo -e ""
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/ajout_auth.sh -o /opt/e-comBox/ajout_auth.sh
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/suppr_auth.sh -o /opt/e-comBox/suppr_auth.sh
bash /opt/e-comBox/ajout_auth.sh

echo -e "$COLCMD"
}

# Détection d'une version antérieure
if docker ps -a | grep e-combox:1.0; then
	clear
        echo -e "$COLTITRE"
        echo "***************************************************"
        echo "*        VERSION 2 de l'e-comBox disponible       *"
        echo "***************************************************"
	echo -e "$COLTXT"
	echo -e "Vous pouvez consulter les nouvelles fonctionnalités ici : http://llb.ac-corse.fr/mw/index.php/La_version_2."
	echo -e "$COLSTOP"
	echo -e "**** ATTENTION ****"
	echo -e ""
        echo -e "La version 2 de l'e-combox est disponible mais le système constate que la version 1 est installée. Les sites de cette version ne sont pas compatibles avec la version 2 et seront supprimés."
        echo -e "$COLTXT"
        echo -e "Voulez-vous mettre à jour la version d'e-comBox (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
        echo -e ""        
        read saisie
        if [ "$saisie" = n ]; then
           # Récupération sur gitlab du fichier qui va effectivement configurer l'application
           curl -fsSL https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/v1/start_configure_application.sh -o start_configure_application_v1.sh
           echo -e ""
           echo -e "$COLTXT"
           echo -e "Votre demande de rester sur la version 1 a été prise en compte. N'hésitez pas à relancer ce script si vous voulez installer la version 2."
           echo -e "Le système va procéder à la reconfiguration de la version 1."

           # Exécution du fichier
           bash start_configure_application_v1.sh

           # Suppression du fichier
           rm start_configure_application_v1.sh
           else 
              # Nettoyage complet des éléments de Docker
              CONTENEURS_ACTIFS=`docker ps -q`

              if [ -n "$CONTENEURS_ACTIFS" ]; then
	         docker stop $CONTENEURS_ACTIFS 2>> /var/log/ecombox_v1-error.log
              fi
              docker system prune -f -a --volumes 2>> /var/log/ecombox_v1-error.log
              echo -e "$COLTXT"
              echo -e "Le système va procéder à la mise à jour de l'e-combox vers la version 2."
              initInstall
              exit 0
           fi
           else 
              if docker ps -a | grep e-combox:dev; then
		 clear     
                 echo -e "$COLTITRE"
                 echo "***************************************************"
                 echo "*        VERSION 2 de l'e-comBox disponible       *"
                 echo "***************************************************"
	         echo -e "$COLSTOP"
		 echo -e "Le système constate que la version de développement est installée, il va la mettre à jour vers la version 2 (stable actuelle). Vos sites seront accessibles à la suite de cette mise à jour."
                 echo -e "$COLTXT"
              fi
	      initInstall
	      exit 0
fi

exit 0