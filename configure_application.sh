#!/bin/bash

# Définition de la branche et de la version des applications
BRANCHE=master
VERSION_APPLI=siollbv2

# Récupération sur gitlab du fichier qui va effectivement configurer l'application
curl -fsSL https://gitlab.com/siollb/e-comBox_scriptsLinux_siollb/raw/$BRANCHE/start_configure_application.sh -o start_configure_application.sh

# Exécution du fichier
bash start_configure_application.sh

# Suppression du fichier
rm start_configure_application.sh
