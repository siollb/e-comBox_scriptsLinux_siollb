#!/bin/bash

# Configuration de l'interface d'authentification

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLSTOP="\033[1;31m"    # Rouge
COLINFO="\033[0;36m"    # Cyan

STOPPER()
{
        echo -e "$COLSTOP"
        echo -e "Vous avez décidé de ne pas configurer l'authentification pour accéder à e-comBox. Vous pourrez reprendre la procédure quand vous voulez."
        echo -e "$1"
        echo -e "$COLTXT"
        exit 1
}


POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
          echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
          read REPONSE
          if [ -z "$REPONSE" ]; then
             REPONSE="o"
          fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
           STOPPER
        fi
}

SAISIRMDP()
{
  
read -rs MDP
if [ -n "$MDP" ]; then
   echo -e "$COLSAISIE\n"
   echo -e "Saisissez de nouveau le mot de passe." 
   read -rs MDPbis
   
   while [ "$MDP" != "$MDPbis" ]
   do
     echo -e "$COLSTOP\n" 
     echo -e "Les mots de passe ne correspondent pas. Il faut recommencer."
     echo -e "$COLSAISIE\n" 
     echo -e "Saisissez le mot de passe."
     read -rs MDP
     echo ""
     echo -e "Saisissez de nouveau le mot de passe." 
     read -rs MDPbis \n 
   done
  fi
}


echo -e "$COLTITRE"
echo "***************************************************"
echo "*       CONFIGURATION DE L'AUTHENTIFICATION       *"
echo "***************************************************"

echo -e "$COLDEFAUT\n"

if [ ! -e /var/lib/docker/volumes/ecombox_config/_data/.auth ]; then            
    echo -e "Vous vous apprếtez à configurer un mot de passe pour accéder à e-comBox. Ce dernier sera associé à un compte \"admin\" préalablement créé. $COLSAISIE\n"
    echo -e "Saisissez le mot de passe (le mot de passe saisi ne s'affiche pas) ou laisser vide pour continuer sans authentification."
    SAISIRMDP
    else
      echo -e "Vous vous apprếtez à modifier le mot de passe associé au compte \"admin\". $COLSAISIE\n"
      echo -e "Saisissez un nouveau mot de passe (le mot de passe saisi ne s'affiche pas) ou laissez vide pour continuer avec l'ancien."
      SAISIRMDP
fi

echo -e "$COLCMD"

if [ ! -n "$MDP" ]; then
   STOPPER
   else
     echo -e "AuthType Basic" > /var/lib/docker/volumes/ecombox_data/_data/.htaccess
     echo -e "AuthName \"E-COMBOX\"" >> /var/lib/docker/volumes/ecombox_data/_data/.htaccess
     echo -e "AuthBasicProvider file" >> /var/lib/docker/volumes/ecombox_data/_data/.htaccess
     echo -e "AuthUserFile /etc/ecombox-conf/.auth" >> /var/lib/docker/volumes/ecombox_data/_data/.htaccess
     echo -e "Require user admin" >> /var/lib/docker/volumes/ecombox_data/_data/.htaccess
     if [ ! -e /var/lib/docker/volumes/ecombox_config/_data/.auth ]; then
        docker exec e-combox htpasswd -cBb /etc/ecombox-conf/.auth admin $MDP
        else
           docker exec e-combox htpasswd -Bb /etc/ecombox-conf/.auth admin $MDP
     fi
fi

echo -e "$COLINFO\n"
echo -e "L'authentification pour accéder à l'interface d'e-comBox a été réalisée."
echo -e ""
echo -e "Vous pouvez la supprimer en exécutant la commande suivante : bash /opt/e-comBox/suppr_auth.sh"
echo -e "$COLCMD"

exit 0

